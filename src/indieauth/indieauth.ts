import { AuthorizationError, AuthenticationError } from './errors.js';

class IndieAuth {
    private logger: (message: string) => void;
    private utils: {
        intersect: (array1: unknown[], array2: unknown[]) => boolean;
    };
    private me: string;
    private tokenEndpoint: string;

    constructor({
        createLogger,
        utils,
        config,
    }: {
        createLogger: (type: string) => (message: string) => void;
        utils: {
            intersect: (array1: unknown[], array2: unknown[]) => boolean;
        };
        config: Config;
    }) {
        this.logger = createLogger('INDIEAUTH');
        this.utils = utils;
        this.me = config.me;
        this.tokenEndpoint = config.tokenEndpoint;
    }

    async isTokenValid(
        token: {
            header?: string;
            body?: string;
        },
        scopes: string[]
    ): Promise<void> {
        if (token.header !== undefined && token.body !== undefined) {
            this.logger('Auth failed: Token in header and body.');
            throw new AuthenticationError(
                'Auth failed: Token in header and body.'
            );
        }
        if (token.header === undefined && token.body === undefined) {
            this.logger('Auth failed: No token.');
            throw new AuthenticationError('Auth failed: No token.');
        }

        const bearerToken = token.header || `bearer ${token.body}`;

        const response = await fetch(this.tokenEndpoint, {
            method: 'GET',
            headers: new Headers({
                Authorization: bearerToken,
                accept: 'application/json',
            }),
        });

        if (response.status === 400) {
            this.logger(
                `Auth failed: ${response.status} - ${response.statusText}`
            );
            throw new AuthenticationError(
                `Auth failed: server returned: ${response.status} - ${response.statusText}`
            );
        }

        if (response.status < 200 || response.status >= 300) {
            this.logger(
                `Auth failed: ${response.status} - ${response.statusText}`
            );
            throw new Error(
                'Invalid response status code recieved when validating token'
            );
        }

        const responseBody = await response.json();

        if (responseBody.me !== this.me) {
            this.logger('Auth failed: "me" doesn\'t match');
            throw new AuthenticationError('Auth failed: URL does not match');
        }

        if (!this.utils.intersect(this.getScopes(responseBody.scope), scopes)) {
            this.logger('Auth failed: required scope/s not present');
            throw new AuthorizationError(
                `Auth failed: Required scope not present: ${scopes}.`
            );
        }
    }

    private getScopes(scopes: undefined | string): string[] {
        if (scopes === undefined) {
            return [];
        }
        return scopes.split(' ');
    }
}

let authSingleton: IndieAuth;
export function createIndieAuth({
    createLogger,
    utils,
    config,
}: {
    createLogger: (type: string) => (message: string) => void;
    utils: {
        intersect: (array1: unknown[], array2: unknown[]) => boolean;
    };
    config: Config;
}): IndieAuth {
    if (authSingleton === undefined) {
        authSingleton = new IndieAuth({ createLogger, utils, config });
    }
    return authSingleton;
}
