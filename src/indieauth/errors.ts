export class AuthenticationError extends Error {
    type = 'Authentication';
}

export class AuthorizationError extends Error {
    type = 'Authorization';
}
