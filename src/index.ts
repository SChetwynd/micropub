import { buildServer } from './server.js';
import { createConfig } from './config.js';
import { createMicropub } from './micropub/index.js';
import { createIndieAuth } from './indieauth/indieauth.js';
import { createGitLabHelper } from './gitlab/gitlab.js';
import { createLogger } from './logger.js';
import { createOpenApiRoute } from './openapi/index.js';
import { createMediaEndpoint } from './media-endpoint/index.js';
import * as utils from './utils/index.js';

const container = {
    indieAuth: () =>
        createIndieAuth({ createLogger, utils, config: container.config() }),
    config: () => createConfig(createLogger),
    gitlabHelper: () =>
        createGitLabHelper(
            container.config().token,
            container.config().projectId,
            container.config().branch,
            createLogger
        ),
    buildServer: () =>
        buildServer(
            container.config(),
            [
                container.createMicropub().router,
                container.createOpenApiRoute(),
                container.createMediaEndpoint().router,
            ],
            createLogger
        ),
    createMediaEndpoint: () =>
        createMediaEndpoint({
            indieAuth: container.indieAuth(),
            createLogger,
            gitlab: container.gitlabHelper(),
            config: container.config(),
        }),
    createMicropub: () =>
        createMicropub({
            indieAuth: container.indieAuth(),
            gitlab: container.gitlabHelper(),
            createLogger,
            config: container.config(),
        }),
    createOpenApiRoute: () =>
        createOpenApiRoute({
            config: container.config(),
            paths: [container.createMicropub().documentation],
        }),
};

container.buildServer();
