import process from 'node:process';
import 'dotenv/config';

const DEFAULT_PORT = 3000;
const DEFAULT_BRANCH = 'master';

let config: Config;

export function createConfig(
    loggerFactory: (type: string) => (message: string) => void
): Config {
    if (config !== undefined) {
        return config;
    }

    const logger = loggerFactory('CONFIG');

    if (process.env.GITLAB_TOKEN === undefined) {
        throw new Error('Requires a GITLAB_TOKEN environment variable');
    }

    if (process.env.PROJECT_ID === undefined) {
        throw new Error('Requires a PROJECT_ID environment variable');
    }

    if (process.env.HOST === undefined) {
        throw new Error('Requires a HOST environment variable');
    }

    if (process.env.ME === undefined) {
        throw new Error('Requires a ME environment variable');
    }

    if (process.env.PORT === undefined) {
        logger(`No port provided, defaulting to: ${DEFAULT_PORT}`);
    }

    if (process.env.BRANCH === undefined) {
        logger(`No branch provided, defaulting to: ${DEFAULT_BRANCH}`);
    }

    if (process.env.MEDIA_ENDPOINT === undefined) {
        logger('No media endpoint provided. Media upload will not be possible');
    }

    if (process.env.TOKEN_ENDPOINT === undefined) {
        throw new Error('Requires a TOKEN_ENDPOINT environment variable.');
    }

    config = {
        projectId: Number(process.env.PROJECT_ID), // the id of the project to commit too
        port: Number(process.env.PORT) || DEFAULT_PORT,
        token: process.env.GITLAB_TOKEN,
        branch: process.env.BRANCH || DEFAULT_BRANCH,
        host: process.env.HOST,
        me: process.env.ME,
        sentryDSN: process.env.SENTRY_DSN,
        mediaEndpoint: process.env.MEDIA_ENDPOINT,
        tokenEndpoint: process.env.TOKEN_ENDPOINT,
    };

    return config;
}
