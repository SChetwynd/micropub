import express from 'express';
import { html } from './api-documentation-ui.html.js';

let router: express.Router;

export function createRouter({
    config,
    paths,
}: {
    config: {
        port: number;
    };
    paths: Record<string, unknown>[];
}): express.Router {
    if (router !== undefined) {
        return router;
    }

    router = express.Router();

    router.get(
        '/api-documentation/ui',
        (_request: express.Request, response: express.Response) => {
            return response.send(html);
        }
    );

    let allPaths = {};
    for (const path of paths) {
        allPaths = { ...allPaths, ...path };
    }

    router.get(
        '/api-documentation/openapi.json',
        (_request: express.Request, response: express.Response) => {
            return response.json({
                openapi: '3.1.0',
                info: {
                    title: 'API Documentation',
                    version: '0.0.0',
                },
                servers: [
                    {
                        url: `http://localhost:${config.port}`,
                        description: 'For testing on local host',
                    },
                    {
                        url: 'https://micropub.srchetwnd.co.uk',
                        description: 'Hosted site',
                    },
                ],
                paths: allPaths,
                components: {
                    securitySchemes: {
                        headerKey: {
                            type: 'apiKey',
                            name: 'authorization',
                            in: 'header',
                        },
                        bodyKey: {
                            type: 'apiKey',
                            name: 'access_token',
                            in: 'body',
                        },
                    },
                },
                security: ['headerKey'],
            });
        }
    );

    return router;
}
