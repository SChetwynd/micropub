import { createRouter } from './routes.js';
import express from 'express';

export function createOpenApiRoute({
    config,
    paths,
}: {
    config: {
        port: number;
    };
    paths: Record<string, unknown>[];
}): express.Router {
    return createRouter({ config, paths });
}
