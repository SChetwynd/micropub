export interface Parameters {
    indieAuth: IndieAuth;
    createLogger: (moduleMessage: string) => (message: string) => void;
    gitlab: Gitlab;
    config: Config;
}

export interface IndieAuth {
    isTokenValid: (
        token: {
            header?: string;
            body?: string;
        },
        scopes: string[]
    ) => Promise<void>;
}

export interface Gitlab {
    uploadFiles: (
        uploadArray: { content: string; fileName: string }[]
    ) => Promise<string[]>;
}
