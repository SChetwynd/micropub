import fs from 'node:fs';
import express from 'express';
import formidable from 'formidable';
import { MediaEndpoint } from './index.js';

let router: express.Router;

function parseBody(
    request: express.Request
): Promise<{ fields: formidable.Fields; files: formidable.Files }> {
    const form = formidable({});

    return new Promise((resolve, reject) =>
        form.parse(request, (error, fields, files) => {
            if (error) {
                return reject(error);
            }

            return resolve({ fields, files });
        })
    );
}

export function createRouter({
    logger,
    indieAuth,
    gitlab,
    config,
}: MediaEndpoint): express.Router {
    if (router !== undefined) {
        return router;
    }

    logger('Creating Route');

    router = express.Router();

    router.post('/v1/media-endpoint', async (request, response, next) => {
        logger('Request Received');
        try {
            await indieAuth.isTokenValid(
                {
                    header: request.headers.authorization,
                    body:
                        typeof request.body.access_token === 'string'
                            ? request.body.access_token
                            : undefined,
                },
                ['create']
            );

            const { fields, files } = await parseBody(request);

            const filesArray = [];

            for (const fileKey in files) {
                const file = files[fileKey];
                if (Array.isArray(file)) {
                    for (const individualFile of file) {
                        filesArray.push({
                            fileName: individualFile.originalFilename ?? 'void',
                            content: fs.readFileSync(individualFile.filepath, {
                                encoding: 'base64',
                            }),
                        });
                    }
                } else {
                    filesArray.push({
                        fileName: file.originalFilename ?? 'void',
                        content: fs.readFileSync(file.filepath).toString(),
                    });
                }
            }

            const paths = await gitlab.uploadFiles(filesArray);

            const newFileUrl = config.me + paths[0].replace(/^static\//, '');

            response.header('location', newFileUrl);
            return response.sendStatus(202);
        } catch (error) {
            logger(`Error: ${(error as Error).message}`);
            return next(error);
        }
    });

    return router;
}
