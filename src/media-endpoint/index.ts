import express from 'express';

import { Parameters, IndieAuth, Gitlab } from './types';
import { createRouter } from './routes.js';

export class MediaEndpoint {
    indieAuth: IndieAuth;
    logger: (message: string) => void;
    gitlab: Gitlab;
    config: Config;

    constructor({ createLogger, indieAuth, gitlab, config }: Parameters) {
        this.logger = createLogger('MEDIA-ENDPOINT');
        this.indieAuth = indieAuth;
        this.gitlab = gitlab;
        this.config = config;
        this.logger('Creating Media Endpoint');
    }

    get router(): express.Router {
        return createRouter(this);
    }
}

let mediaEndpoint: MediaEndpoint;
export function createMediaEndpoint(parameters: Parameters): MediaEndpoint {
    if (mediaEndpoint === undefined) {
        mediaEndpoint = new MediaEndpoint(parameters);
    }
    return mediaEndpoint;
}
