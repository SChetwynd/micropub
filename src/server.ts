import express from 'express';
import bodyParser from 'body-parser';
import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';

interface CustomError extends Error {
    type: string;
}

let app: express.Express;

export function buildServer(
    config: Config,
    routes: express.Router[],
    createLogger: (type: string) => (message: string) => void
): void {
    if (app !== undefined) {
        return;
    }

    const logger = createLogger('SERVER');

    app = express();

    if (config.sentryDSN !== undefined) {
        Sentry.init({
            dsn: config.sentryDSN,
            integrations: [
                new Sentry.Integrations.Http({ tracing: true }),
                new Tracing.Integrations.Express({ app }),
            ],
        });
        app.use(Sentry.Handlers.requestHandler() as express.RequestHandler);
        logger('Sentry Initialised');
    } else {
        logger('No Sentry DSN, Sentry not initialised');
    }

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.use(routes);

    app.use((_request, _response, next) => next(new Error('Not Found')));

    if (config.sentryDSN !== undefined) {
        app.use(Sentry.Handlers.errorHandler() as express.ErrorRequestHandler);
    }

    app.use(
        (
            error: Error | CustomError,
            _request: express.Request,
            response: express.Response,
            _next: express.NextFunction
        ) => {
            if (
                'type' in error &&
                (error.type === 'Authentication' ||
                    error.type === 'Authrorization')
            ) {
                return response.status(403).send(error.message);
            }

            return response.sendStatus(500);
        }
    );

    app.listen(config.port, () => {
        logger(`Server listening on port: ${config.port}`);
    });
}
