import debug from 'debug';

export function createLogger(type: string): debug.Debugger {
    return debug(`MICROPUB:${type}`);
}
