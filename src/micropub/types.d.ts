export type Photo =
    | string[]
    | {
          value: string;
          alt?: string;
      }[];

export interface IndieAuth {
    isTokenValid: (
        token: {
            header?: string;
            body?: string;
        },
        scopes: string[]
    ) => Promise<void>;
}

export interface Gitlab {
    createCommit: (body: string) => Promise<string>;
}

export interface Parameters {
    indieAuth: IndieAuth;
    gitlab: Gitlab;
    createLogger: (moduleMessage: string) => (message: string) => void;
    config: Config;
}

export interface AdrJson {
    access_token?: string;
    type: ['h-adr'];
    properties: {
        'post-office-box'?: string;
        'extended-address'?: string;
        'street-addres'?: string;
        'postal-code'?: string;
        'country-name'?: string;
        locality?: string;
        region?: string;
        label?: string;
        latitude?: string;
        longitude?: string;
        altitude?: string;
    };
}

export interface CardJson {
    access_token?: string;
    type: ['h-card'];
    properties: {
        name?: string;
        'honorific-prefix'?: string;
        'given-name'?: string;
        'additional-name'?: string;
        'family-name'?: string;
        'sort-string'?: string;
        'honorific-suffix'?: string;
        nickname?: string;
        email?: string;
        logo?: string;
        photo?: Photo;
        url?: string;
        uid?: string;
        category?: string[];
        adr?: string;
        'post-office-box'?: string;
        'extended-address'?: string;
        'street-address'?: string;
        locality?: string;
        region?: string;
        'postal-code'?: string;
        'contry-name'?: string;
        label?: string;
        geo?: string;
        latitude?: string;
        longitude?: string;
        altitude?: string;
        tel?: string;
        note?: string;
        bday?: string;
        key?: string;
        'job-title'?: string;
        role?: string;
        impp?: string;
        sex?: string;
        'gender-identity'?: string;
        anniversary?: string;
        'organization-name'?: string;
        'organization-unit'?: string;
        tz?: string;
        rev?: string;
    };
}

export interface EntryJson {
    access_token?: string;
    type: ['h-entry'];
    properties: {
        name?: string;
        summary?: string;
        content?: string[] | { value?: string; html?: string }[];
        published?: string;
        updated?: string;
        author?: string;
        location?: string[];
        category?: string[];
        url?: string;
        geo?: string;
        latitude?: string;
        longitude?: string;
        photo?: Photo;
        audio?: string;
        video?: string;
        'in-reply-to'?: string;
        'like-of'?: string;
        'repost-of'?: string;
        'bookmark-of'?: string;
        'read-of'?: string;
        rsvp?: string;
        checkin?: string;
    };
}

export interface EventJson {
    access_token?: string;
    type: ['h-event'];
    properties: {
        name?: string;
        summary?: string;
        start?: string;
        end?: string;
        duration?: string;
        description?: string;
        url?: string;
        category?: string[];
        location?: string;
        geo?: string;
        latitude?: string;
        longitude?: string;
    };
}

export interface GeoJson {
    access_token?: string;
    type: ['h-geo'];
    properties: Location;
}

export interface Location {
    latitude?: string;
    longitude?: string;
    altitude?: string;
    location?: string[];
}

export interface ItemJson {
    access_token?: string;
    type: ['h-item'];
    properties: {
        name?: string;
        photo?: Photo;
        url?: string;
    };
}

export interface ProductJson {
    access_token?: string;
    type: ['h-product'];
    properties: {
        name?: string;
        photo?: Photo;
        brand?: string;
        category?: string[];
        description?: string;
        url?: string;
        identifier?: string;
        review?: string;
        price?: string;
    };
}

export interface RecipeJson {
    access_token?: string;
    type: ['h-recipe'];
    properties: {
        ingredient?: string;
        yield?: string;
        instructions?: string;
        duration?: string;
        photo?: Photo;
        summary?: string;
        author?: string;
        published?: string;
        nutrition?: string;
    };
}

export interface ResumeJson {
    access_token?: string;
    type: ['h-resume'];
    properties: {
        summary?: string;
        contact?: string;
        education?: string;
        experience?: string;
        skill?: string;
        affiliation?: string;
    };
}

export interface ReviewJson {
    access_token?: string;
    type: ['h-review'];
    properties: {
        name?: string;
        item?: string;
        reviewer?: string;
        reviewed?: string;
        rating?: number;
        best?: number;
        worst?: number;
        description?: string;
        category?: string[];
        url?: string;
    };
}

export interface ReviewAggregateJson {
    access_token?: string;
    type: ['h-review-aggregate'];
    properties: {
        name?: string;
        item?: string;
        rating?: number;
        best?: number;
        worst?: number;
        count?: number;
        votes?: number;
        category?: string[];
        url?: string;
    };
}

export type MicropubJson =
    | AdrJson
    | CardJson
    | EntryJson
    | EventJson
    | GeoJson
    | ItemJson
    | ProductJson
    | RecipeJson
    | ResumeJson
    | ReviewJson
    | ReviewAggregateJson;

export interface AdrForm {
    h: 'adr';
    'post-office-box'?: string;
    'extended-address'?: string;
    'street-address'?: string;
    locality?: string;
    region?: string;
    'postal-code'?: string;
    'country-name'?: string;
    label?: string;
    latitude?: string;
    longitude?: string;
    altitude?: string;
}

export interface CardForm {
    h: 'card';
    name?: string;
    'honorific-prefix'?: string;
    'given-name'?: string;
    'additional-name'?: string;
    'family-name'?: string;
    'sort-string'?: string;
    'honorific-suffix'?: string;
    nickname?: string;
    email?: string;
    logo?: string;
    photo?: Photo;
    url?: string;
    uid?: string;
    category?: string[];
    adr?: string;
    'post-office-box'?: string;
    'extended-address'?: string;
    'street-address'?: string;
    locality?: string;
    region?: string;
    'postal-code'?: string;
    'contry-name'?: string;
    label?: string;
    geo?: string;
    latitude?: string;
    longitude?: string;
    altitude?: string;
    tel?: string;
    note?: string;
    bday?: string;
    key?: string;
    'job-title'?: string;
    role?: string;
    impp?: string;
    sex?: string;
    'gender-identity'?: string;
    anniversary?: string;
    'organization-name'?: string;
    'organization-unit'?: string;
    tz?: string;
    rev?: string;
}

export interface EntryForm {
    h: 'entry';
    name?: string;
    summary?: string;
    content?: string[];
    published?: string;
    updated?: string;
    author?: string;
    location?: string;
    category?: string[];
    url?: string;
    geo?: string;
    latitude?: string;
    longitude?: string;
    photo?: Photo;
    audio?: string;
    video?: string;
    'in-reply-to'?: string;
    'repost-of'?: string;
    'bookmark-of'?: string;
    'read-of'?: string;
    rsvp?: string;
    checkin?: string;
}

export interface EventForm {
    h: 'event';
    name?: string;
    summary?: string;
    start?: string;
    end?: string;
    duration?: string;
    description?: string;
    url?: string;
    category?: string[];
    location?: string;
    geo?: string;
    latitude?: string;
    longitude?: string;
}

export interface GeoForm {
    h: 'geo';
    latitude?: string;
    longitude?: string;
    altitude?: string;
}

export interface ItemForm {
    h: 'item';
    name?: string;
    photo?: Photo;
    url?: string;
}

export interface ProductForm {
    h: 'product';
    name?: string;
    photo?: Photo;
    brand?: string;
    category?: string[];
    description?: string;
    url?: string;
    identifier?: string;
    review?: string;
    price?: string;
}

export interface RecipeForm {
    h: 'recipe';
    ingredient?: string;
    yield?: string;
    instructions?: string;
    duration?: string;
    photo?: Photo;
    summary?: string;
    author: string;
    published?: string;
    nutrition?: string;
}

export interface ResumeForm {
    h: 'resume';
    summary?: string;
    contact?: string;
    education?: string;
    experience?: string;
    skill?: string;
    affiliation?: string;
}

export interface ReviewForm {
    h: 'review';
    name?: string;
    item?: string;
    reviewer?: string;
    reviewed?: string;
    rating?: number;
    best?: number;
    worst?: number;
    description?: string;
    category?: string[];
    url?: string;
}

export interface ReviewAggregateForm {
    h: 'review-aggregate';
    name?: string;
    item?: string;
    rating?: number;
    best?: number;
    worst?: number;
    count?: number;
    votes?: number;
    category?: string[];
    url?: string;
}

export type MicropubForm =
    | AdrForm
    | CardForm
    | EntryForm
    | EventForm
    | GeoForm
    | ItemForm
    | ProductForm
    | RecipeForm
    | ResumeForm
    | ReviewForm
    | ReviewAggregateForm;

export type Micropub = MicropubJson | MicropubForm;

export interface FrontMatter {
    title?: string;
    location?: {
        latitude: string;
        longitude: string;
        altitude?: string;
    };
    tags?: string[];
    description?: string;
    date: string;
    draft: false;
    disableComments: true;
    image?: string;
}
