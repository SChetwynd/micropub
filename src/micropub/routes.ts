import express from 'express';
import { formatPost } from './format-post.js';
import { Micropub } from './index.js';
import { standardiseBody, convertArrays } from './standardiser.js';

let router: express.Router;

export function createRouter({
    indieAuth,
    gitlab,
    logger,
    validation,
    config,
}: Micropub): express.Router {
    if (router !== undefined) {
        return router;
    }
    router = express.Router();

    router.get('/v1/micropub', async (request, response, next) => {
        logger('Query Request Recieved');
        try {
            if (request.query.q === undefined) {
                throw new Error('No query provided');
            }

            if (request.query.q !== 'config') {
                throw new Error(`Unsupported query type: "${request.query.q}"`);
            }

            return response.json({
                'media-endpoint': config.mediaEndpoint,
            });
        } catch (error) {
            logger(`Error: ${(error as Error).message}`);
            return next(error);
        }
    });

    router.post('/v1/micropub', async (request, response, next) => {
        logger(
            `Request Recieved: ${JSON.stringify(request.body, undefined, 4)}`
        );
        try {
            const body = convertArrays(request.body);

            if (!validation.validate(body)) {
                throw new Error(
                    'Error: it should not be possible to reach here'
                );
            }

            const standardisedBody = standardiseBody(body);

            await indieAuth.isTokenValid(
                {
                    header: request.headers.authorization,
                    body:
                        typeof body.access_token === 'string'
                            ? body.access_token
                            : undefined,
                },
                ['create']
            );

            const post = formatPost(standardisedBody);

            const filePath = await gitlab.createCommit(post);

            const newPostUrl =
                config.me +
                filePath.replace(/^content\//, '').replace(/\.md$/, '/');

            response.header('location', newPostUrl);

            return response.sendStatus(202);
        } catch (error) {
            logger(`Error: ${(error as Error).message}`);
            return next(error);
        }
    });

    return router;
}
