import {
    Micropub,
    MicropubForm,
    AdrForm,
    CardForm,
    EntryForm,
    EventForm,
    GeoForm,
    ItemForm,
    ProductForm,
    RecipeForm,
    ResumeForm,
    ReviewForm,
    ReviewAggregateForm,
    MicropubJson,
    AdrJson,
    CardJson,
    EntryJson,
    EventJson,
    GeoJson,
    ItemJson,
    ProductJson,
    RecipeJson,
    ResumeJson,
    ReviewJson,
    ReviewAggregateJson,
} from './types';

const H_CLASSES_SET = new Set([
    'adr',
    'card',
    'entry',
    'event',
    'geo',
    'item',
    'product',
    'recipe',
    'resume',
    'review',
    'review-aggregate',
]);

export function isMicorpubForm(body: Micropub): body is MicropubForm {
    return H_CLASSES_SET.has((body as MicropubForm).h);
}

export function isMicropubJson(body: Micropub): body is MicropubJson {
    return 'type' in body && Array.isArray(body.type);
}

export function isAdrForm(body: MicropubForm): body is AdrForm {
    return body.h === 'adr';
}

export function isCardForm(body: MicropubForm): body is CardForm {
    return body.h === 'card';
}

export function isEntryForm(body: MicropubForm): body is EntryForm {
    return body.h === 'entry';
}

export function isEventForm(body: MicropubForm): body is EventForm {
    return body.h === 'event';
}

export function isGeoForm(body: MicropubForm): body is GeoForm {
    return body.h === 'geo';
}

export function isItemForm(body: MicropubForm): body is ItemForm {
    return body.h === 'item';
}

export function isProductForm(body: MicropubForm): body is ProductForm {
    return body.h === 'product';
}

export function isRecipeForm(body: MicropubForm): body is RecipeForm {
    return body.h === 'recipe';
}

export function isResumeForm(body: MicropubForm): body is ResumeForm {
    return body.h === 'resume';
}

export function isReviewForm(body: MicropubForm): body is ReviewForm {
    return body.h === 'review';
}

export function isReviewAggregateForm(
    body: MicropubForm
): body is ReviewAggregateForm {
    return body.h === 'review-aggregate';
}

export function isAdrJson(body: MicropubJson): body is AdrJson {
    return body.type[0] === 'h-adr';
}

export function isCardJson(body: MicropubJson): body is CardJson {
    return body.type[0] === 'h-card';
}

export function isEntryJson(body: MicropubJson): body is EntryJson {
    return body.type[0] === 'h-entry';
}

export function isEventJson(body: MicropubJson): body is EventJson {
    return body.type[0] === 'h-event';
}

export function isGeoJson(body: MicropubJson): body is GeoJson {
    return body.type[0] === 'h-geo';
}

export function isItemJson(body: MicropubJson): body is ItemJson {
    return body.type[0] === 'h-item';
}

export function isProductJson(body: MicropubJson): body is ProductJson {
    return body.type[0] === 'h-product';
}

export function isRecipeJson(body: MicropubJson): body is RecipeJson {
    return body.type[0] === 'h-recipe';
}

export function isResumeJson(body: MicropubJson): body is ResumeJson {
    return body.type[0] === 'h-resume';
}

export function isReviewJson(body: MicropubJson): body is ReviewJson {
    return body.type[0] === 'h-review';
}

export function isReviewAggregateJson(
    body: MicropubJson
): body is ReviewAggregateJson {
    return body.type[0] === 'h-review-aggregate';
}
