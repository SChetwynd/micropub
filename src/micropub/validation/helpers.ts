interface IfThen {
    if: Record<string, unknown>;
    then: Record<string, unknown>;
}

export function embedIfElses(
    ifThens: IfThen[]
): Record<string, unknown> | false {
    const currentIf = ifThens.pop();

    if (currentIf === undefined) {
        return false;
    }

    return {
        if: currentIf.if,
        then: currentIf.then,
        else: embedIfElses(ifThens),
    };
}
