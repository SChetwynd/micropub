import { embedIfElses } from './helpers.js';

export function buildJsonValidation({
    host,
}: {
    host: string;
}): Record<string, unknown>[] {
    const adr = {
        $id: `${host}/v1/json/adr.json`,
        type: 'object',
        properties: {
            'post-office-box': {
                type: 'string',
            },
            'extended-address': {
                type: 'string',
            },
            'street-addres': {
                type: 'string',
            },
            locality: {
                type: 'string',
            },
            region: {
                type: 'string',
            },
            'postal-code': {
                type: 'string',
            },
            'country-name': {
                type: 'string',
            },
            label: {
                type: 'string',
            },
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
            altitude: {
                type: 'string',
            },
        },
    };

    const card = {
        $id: `${host}/v1/json/card.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            'honorific-prefix': {
                type: 'string',
            },
            'given-name': {
                type: 'string',
            },
            'additional-name': {
                type: 'string',
            },
            'family-name': {
                type: 'string',
            },
            'sort-string': {
                type: 'string',
            },
            'honorific-suffix': {
                type: 'string',
            },
            nickname: {
                type: 'string',
            },
            email: {
                type: 'string',
            },
            logo: {
                type: 'string',
            },
            photo: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        value: {
                            type: 'string',
                        },
                        alt: {
                            type: 'string',
                        },
                    },
                },
            },
            url: {
                type: 'string',
            },
            uid: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            adr: {
                type: 'string',
            },
            'post-office-box': {
                type: 'string',
            },
            'extended-address': {
                type: 'string',
            },
            'street-address': {
                type: 'string',
            },
            locality: {
                type: 'string',
            },
            region: {
                type: 'string',
            },
            'postal-code': {
                type: 'string',
            },
            'contry-name': {
                type: 'string',
            },
            label: {
                type: 'string',
            },
            geo: {
                type: 'string',
            },
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
            altitude: {
                type: 'string',
            },
            tel: {
                type: 'string',
            },
            note: {
                type: 'string',
            },
            bday: {
                type: 'string',
            },
            key: {
                type: 'string',
            },
            'job-title': {
                type: 'string',
            },
            role: {
                type: 'string',
            },
            impp: {
                type: 'string',
            },
            sex: {
                type: 'string',
            },
            'gender-identity': {
                type: 'string',
            },
            anniversary: {
                type: 'string',
            },
            'organization-name': {
                type: 'string',
            },
            'organization-unit': {
                type: 'string',
            },
            tz: {
                type: 'string',
            },
            rev: {
                type: 'string',
            },
        },
    };

    const entry = {
        $id: `${host}/v1/json/entry.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            summary: {
                type: 'string',
            },
            content: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        html: {
                            type: 'string',
                        },
                        value: {
                            type: 'string',
                        },
                    },
                },
            },
            published: {
                type: 'string',
            },
            updated: {
                type: 'string',
            },
            author: {
                type: 'string',
            },
            location: {
                type: 'array',
                items: {
                    type: 'string',
                },
            },
            category: {
                type: ['string', 'array'],
                items: {
                    type: 'string',
                },
            },
            url: {
                type: 'string',
            },
            geo: {
                type: 'string',
            },
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
            photo: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        value: {
                            type: 'string',
                        },
                        alt: {
                            type: 'string',
                        },
                    },
                },
            },
            audio: {
                type: 'string',
            },
            video: {
                type: 'string',
            },
            'in-reply-to': {
                type: 'string',
            },
            'like-of': {
                type: 'string',
            },
            'repost-of': {
                type: 'string',
            },
            'bookmark-of': {
                type: 'string',
            },
            'read-of': {
                type: 'string',
            },
            rsvp: {
                type: 'string',
            },
            checkin: {
                type: 'string',
            },
        },
    };

    const event = {
        $id: `${host}/v1/json/event.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            summary: {
                type: 'string',
            },
            start: {
                type: 'string',
            },
            end: {
                type: 'string',
            },
            duration: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
            url: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            location: {
                type: 'array',
                items: {
                    type: 'string',
                },
            },
            geo: {
                type: 'string',
            },
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
        },
    };

    const geo = {
        $id: `${host}/v1/json/geo.json`,
        type: 'object',
        properties: {
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
            altitude: {
                type: 'string',
            },
        },
    };

    const item = {
        $id: `${host}/v1/json/item.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            photo: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        value: {
                            type: 'string',
                        },
                        alt: {
                            type: 'string',
                        },
                    },
                },
            },
            url: {
                type: 'string',
            },
        },
    };

    const product = {
        $id: `${host}/v1/json/product.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            photo: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        value: {
                            type: 'string',
                        },
                        alt: {
                            type: 'string',
                        },
                    },
                },
            },
            brand: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            description: {
                type: 'string',
            },
            url: {
                type: 'string',
            },
            identifier: {
                type: 'string',
            },
            review: {
                type: 'string',
            },
            price: {
                type: 'string',
            },
        },
    };

    const recipe = {
        $id: `${host}/v1/json/recipe.json`,
        type: 'object',
        properties: {
            ingredient: {
                type: 'string',
            },
            yield: {
                type: 'string',
            },
            instructions: {
                type: 'string',
            },
            duration: {
                type: 'string',
            },
            photo: {
                type: 'array',
                items: {
                    type: ['string', 'object'],
                    properties: {
                        value: {
                            type: 'string',
                        },
                        alt: {
                            type: 'string',
                        },
                    },
                },
            },
            summary: {
                type: 'string',
            },
            author: {
                type: 'string',
            },
            published: {
                type: 'string',
            },
            nutrition: {
                type: 'string',
            },
        },
    };

    const resume = {
        $id: `${host}/v1/json/resume.json`,
        type: 'object',
        properties: {
            summary: {
                type: 'string',
            },
            contact: {
                type: 'string',
            },
            education: {
                type: 'string',
            },
            experience: {
                type: 'string',
            },
            skill: {
                type: 'string',
            },
            affiliation: {
                type: 'string',
            },
        },
    };

    const review = {
        $id: `${host}/v1/json/review.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            item: {
                type: 'string',
            },
            reviewer: {
                type: 'string',
            },
            reviewed: {
                type: 'string',
            },
            rating: {
                type: 'number',
            },
            best: {
                type: 'number',
            },
            worst: {
                type: 'number',
            },
            description: {
                type: 'string',
            },
            category: {
                type: 'string',
            },
            url: {
                type: 'string',
            },
        },
    };

    const reviewAggregate = {
        $id: `${host}/v1/json/review-aggregate.json`,
        type: 'object',
        properties: {
            name: {
                type: 'string',
            },
            item: {
                type: 'string',
            },
            rating: {
                type: 'number',
            },
            best: {
                type: 'number',
            },
            worst: {
                type: 'number',
            },
            count: {
                type: 'number',
            },
            votes: {
                type: 'number',
            },
            category: {
                type: 'string',
            },
            url: {
                type: 'string',
            },
        },
    };

    const main = {
        $id: `${host}/v1/json/micro-formats.json`,
        type: 'object',
        required: ['type', 'properties'],
        properties: {
            type: {
                type: 'array',
                items: {
                    type: 'string',
                    enum: [
                        'h-adr',
                        'h-card',
                        'h-entry',
                        'h-event',
                        'h-geo',
                        'h-item',
                        'h-product',
                        'h-recipe',
                        'h-resume',
                        'h-review',
                        'h-reviewAggregate',
                    ],
                },
            },
        },
        ...embedIfElses([
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-adr',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/adr.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-card',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/card.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-entry',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/entry.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-event',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/event.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-geo',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/geo.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-item',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/item.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-product',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/product.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-recipe',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/recipe.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-resume',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/resume.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-review',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/review.json`,
                        },
                    },
                },
            },
            {
                if: {
                    properties: {
                        type: {
                            contains: {
                                type: 'string',
                                const: 'h-review-aggregate',
                            },
                        },
                    },
                },
                then: {
                    properties: {
                        properties: {
                            $ref: `${host}/v1/json/review-aggregate.json`,
                        },
                    },
                },
            },
        ]),
    };

    return [
        adr,
        card,
        entry,
        event,
        geo,
        item,
        product,
        recipe,
        resume,
        review,
        reviewAggregate,
        main,
    ];
}
