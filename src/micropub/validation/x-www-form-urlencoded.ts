import { embedIfElses } from './helpers.js';

export function buildFormValidation({
    host,
}: {
    host: string;
}): Record<string, unknown>[] {
    const adr = {
        $id: `${host}/v1/x-www-form-urlencode/adr.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'adr',
            },
            'p-post-office-box': {
                type: 'string',
            },
            'p-extended-address': {
                type: 'string',
            },
            'p-street-addres': {
                type: 'string',
            },
            'p-locality': {
                type: 'string',
            },
            'p-region': {
                type: 'string',
            },
            'p-postal-code': {
                type: 'string',
            },
            'p-country-name': {
                type: 'string',
            },
            'p-label': {
                type: 'string',
            },
            'p-latitude': {
                type: 'string',
            },
            'p-longitude': {
                type: 'string',
            },
            'p-altitude': {
                type: 'string',
            },
        },
    };

    const card = {
        $id: `${host}/v1/x-www-form-urlencode/card.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'card',
            },
            'p-name': {
                type: 'string',
            },
            'p-honorific-prefix': {
                type: 'string',
            },
            'p-given-name': {
                type: 'string',
            },
            'p-additional-name': {
                type: 'string',
            },
            'p-family-name': {
                type: 'string',
            },
            'p-sort-string': {
                type: 'string',
            },
            'p-honorific-suffix': {
                type: 'string',
            },
            'p-nickname': {
                type: 'string',
            },
            'p-email': {
                type: 'string',
            },
            'p-logo': {
                type: 'string',
            },
            'p-photo': {
                type: 'string',
            },
            'p-url': {
                type: 'string',
            },
            'p-uid': {
                type: 'string',
            },
            'p-category': {
                type: 'string',
            },
            'p-adr': {
                type: 'string',
            },
            'p-post-office-box': {
                type: 'string',
            },
            'p-extended-address': {
                type: 'string',
            },
            'p-street-address': {
                type: 'string',
            },
            'p-locality': {
                type: 'string',
            },
            'p-region': {
                type: 'string',
            },
            'p-postal-code': {
                type: 'string',
            },
            'p-contry-name': {
                type: 'string',
            },
            'p-label': {
                type: 'string',
            },
            'p-geo': {
                type: 'string',
            },
            'u-geo': {
                type: 'string',
            },
            'p-latitude': {
                type: 'string',
            },
            'p-longitude': {
                type: 'string',
            },
            'p-altitude': {
                type: 'string',
            },
            'p-tel': {
                type: 'string',
            },
            'p-note': {
                type: 'string',
            },
            'bt-bday': {
                type: 'string',
            },
            'u-key': {
                type: 'string',
            },
            'p-job-title': {
                type: 'string',
            },
            'p-role': {
                type: 'string',
            },
            'p-impp': {
                type: 'string',
            },
            'p-sex': {
                type: 'string',
            },
            'p-gender-identity': {
                type: 'string',
            },
            'dt-anniversary': {
                type: 'string',
            },
            'p-organization-name': {
                type: 'string',
            },
            'p-organization-unit': {
                type: 'string',
            },
            'p-tz': {
                type: 'string',
            },
            'dt-rev': {
                type: 'string',
            },
        },
    };

    const entry = {
        $id: `${host}/v1/x-www-form-urlencode/entry.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'entry',
            },
            name: {
                type: 'string',
            },
            summary: {
                type: 'string',
            },
            content: {
                type: 'string',
            },
            published: {
                type: 'string',
            },
            updated: {
                type: 'string',
            },
            author: {
                type: 'string',
            },
            location: {
                type: 'string',
            },
            category: {
                type: ['string', 'array'],
                items: {
                    type: 'string',
                },
            },
            url: {
                type: 'string',
            },
            geo: {
                type: 'string',
            },
            latitude: {
                type: 'string',
            },
            longitude: {
                type: 'string',
            },
            photo: {
                type: ['string', 'array'],
                items: {
                    type: 'string',
                },
            },
            audio: {
                type: 'string',
            },
            video: {
                type: 'string',
            },
            'in-reply-to': {
                type: 'string',
            },
            'like-of': {
                type: 'string',
            },
            'repost-of': {
                type: 'string',
            },
            'bookmark-of': {
                type: 'string',
            },
            'read-of': {
                type: 'string',
            },
            rsvp: {
                type: 'string',
            },
            checkin: {
                type: 'string',
            },
        },
    };

    const event = {
        $id: `${host}/v1/x-www-form-urlencode/event.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'event',
            },
            'p-name': {
                type: 'string',
            },
            'p-summary': {
                type: 'string',
            },
            'dt-start': {
                type: 'string',
            },
            'dt-end': {
                type: 'string',
            },
            'dt-duration': {
                type: 'string',
            },
            'p-description': {
                type: 'string',
            },
            'u-url': {
                type: 'string',
            },
            'p-category': {
                type: 'string',
            },
            'p-location': {
                type: 'string',
            },
            'p-geo': {
                type: 'string',
            },
            'p-latitude': {
                type: 'string',
            },
            'p-longitude': {
                type: 'string',
            },
        },
    };

    const geo = {
        $id: `${host}/v1/x-www-form-urlencode/geo.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'geo',
            },
            'p-latitude': {
                type: 'string',
            },
            'p-longitude': {
                type: 'string',
            },
            'p-altitude': {
                type: 'string',
            },
        },
    };

    const item = {
        $id: `${host}/v1/x-www-form-urlencode/item.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'item',
            },
            'p-name': {
                type: 'string',
            },
            'u-photo': {
                type: 'string',
            },
            'u-url': {
                type: 'string',
            },
        },
    };

    const product = {
        $id: `${host}/v1/x-www-form-urlencode/product.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'product',
            },
            'p-name': {
                type: 'string',
            },
            'u-photo': {
                type: 'string',
            },
            'p-brand': {
                type: 'string',
            },
            'p-category': {
                type: 'string',
            },
            'e-description': {
                type: 'string',
            },
            'u-url': {
                type: 'string',
            },
            'u-identifier': {
                type: 'string',
            },
            'p-review': {
                type: 'string',
            },
            'p-price': {
                type: 'string',
            },
        },
    };

    const recipe = {
        $id: `${host}/v1/x-www-form-urlencode/recipe.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'recipe',
            },
            'p-ingredient': {
                type: 'string',
            },
            'p-yield': {
                type: 'string',
            },
            'e-instructions': {
                type: 'string',
            },
            'dt-duration': {
                type: 'string',
            },
            'u-photo': {
                type: 'string',
            },
            'p-summary': {
                type: 'string',
            },
            'p-author': {
                type: 'string',
            },
            'dt-published': {
                type: 'string',
            },
            'p-nutrition': {
                type: 'string',
            },
        },
    };

    const resume = {
        $id: `${host}/v1/x-www-form-urlencode/resume.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'resume',
            },
            'p-summary': {
                type: 'string',
            },
            'p-contact': {
                type: 'string',
            },
            'p-education': {
                type: 'string',
            },
            'p-experience': {
                type: 'string',
            },
            'p-skill': {
                type: 'string',
            },
            'p-affiliation': {
                type: 'string',
            },
        },
    };

    const review = {
        $id: `${host}/v1/x-www-form-urlencode/review.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'review',
            },
            'p-name': {
                type: 'string',
            },
            'p-item': {
                type: 'string',
            },
            'p-reviewer': {
                type: 'string',
            },
            'dt-reviewed': {
                type: 'string',
            },
            'p-rating': {
                type: 'number',
            },
            'p-best': {
                type: 'number',
            },
            'p-worst': {
                type: 'number',
            },
            'e-description': {
                type: 'string',
            },
            'p-category': {
                type: 'string',
            },
            'u-url': {
                type: 'string',
            },
        },
    };

    const reviewAggregate = {
        $id: `${host}/v1/x-www-form-urlencode/review-aggregate.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                const: 'review',
            },
            'p-name': {
                type: 'string',
            },
            'p-item': {
                type: 'string',
            },
            'p-rating': {
                type: 'number',
            },
            'p-best': {
                type: 'number',
            },
            'p-worst': {
                type: 'number',
            },
            'p-count': {
                type: 'number',
            },
            'p-votes': {
                type: 'number',
            },
            'p-category': {
                type: 'string',
            },
            'u-url': {
                type: 'string',
            },
        },
    };

    const microFormats = {
        $id: `${host}/v1/x-www-form-urlencode/micro-formats.json`,
        type: 'object',
        required: ['h'],
        properties: {
            h: {
                type: 'string',
                enum: [
                    'adr',
                    'card',
                    'entry',
                    'event',
                    'geo',
                    'item',
                    'product',
                    'recipe',
                    'resume',
                    'review',
                    'review-aggregate',
                ],
            },
        },
        ...embedIfElses([
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'adr',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/adr.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'card',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/card.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'entry',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/entry.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'event',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/event.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'geo',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/geo.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'item',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/item.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'product',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/product.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'recipe',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/recipe.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'resume',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/resume.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'review',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/review.json`,
                },
            },
            {
                if: {
                    properties: {
                        h: {
                            type: 'string',
                            const: 'review-aggregate',
                        },
                    },
                },
                then: {
                    $ref: `${host}/v1/x-www-form-urlencode/review-aggregate.json`,
                },
            },
        ]),
    };

    return [
        adr,
        card,
        entry,
        event,
        geo,
        item,
        product,
        recipe,
        resume,
        review,
        reviewAggregate,
        microFormats,
    ];
}
