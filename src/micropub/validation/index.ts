import Ajv from 'ajv';
import addFormats from 'ajv-formats';

import { buildJsonValidation } from './json.js';
import { buildFormValidation } from './x-www-form-urlencoded.js';

import { Micropub } from '../types';

class ValidationError extends Error {
    details: unknown;
}

export class Validation {
    private config: { host: string };
    private ajv: Ajv;
    private validator;
    public schemas: Record<string, unknown>[];

    constructor(config: { host: string }) {
        this.config = config;

        this.schemas = [
            ...buildJsonValidation(this.config),
            ...buildFormValidation(this.config),
        ];

        this.ajv = new Ajv();
        addFormats(this.ajv);
        this.ajv.addSchema(this.schemas);
        this.validator = this.ajv.compile({
            if: {
                required: ['h'],
            },
            then: {
                $ref: `${this.config.host}/v1/x-www-form-urlencode/micro-formats.json`,
            },
            else: {
                $ref: `${this.config.host}/v1/json/micro-formats.json`,
            },
        });
    }

    validate(message: unknown): message is Micropub {
        const validationResult = this.validator(message);
        if (!validationResult) {
            const error = new ValidationError('Invalid Message');
            error.details = this.validator.errors;
            throw error;
        }
        return true;
    }
}

let validator: Validation;

export function buildValidation(config: { host: string }): Validation {
    if (validator === undefined) {
        validator = new Validation(config);
    }
    return validator;
}
