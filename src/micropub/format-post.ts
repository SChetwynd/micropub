import { MicropubJson, FrontMatter, EntryJson } from './types';
import {
    isAdrJson,
    isCardJson,
    isEntryJson,
    isEventJson,
    isGeoJson,
    isItemJson,
    isProductJson,
    isRecipeJson,
    isResumeJson,
    isReviewJson,
    isReviewAggregateJson,
} from './guards.js';

export function formatPost(data: MicropubJson): string {
    const frontMatter = formatFrontMatter(data);

    return frontMatter + formatContent(data);
}

function formatContent(data: MicropubJson): string {
    if (isAdrJson(data)) {
        let content = '';

        if (data.properties.label !== undefined) {
            content += `Label: ${data.properties.label}\n\n`;
        }

        if (data.properties['post-office-box'] !== undefined) {
            content += `PO Box: ${data.properties['post-office-box']}\n\n`;
        }

        if (data.properties['extended-address'] !== undefined) {
            content += data.properties['extended-address'];
            content += '\n\n';
        }

        if (data.properties['street-addres'] !== undefined) {
            content += data.properties['street-addres'];
            content += '\n\n';
        }

        if (data.properties.locality !== undefined) {
            content += data.properties.locality;
            content += '\n\n';
        }

        if (data.properties.region !== undefined) {
            content += data.properties.region;
            content += '\n\n';
        }

        if (data.properties['postal-code'] !== undefined) {
            content += data.properties['postal-code'];
            content += '\n\n';
        }

        if (data.properties['country-name'] !== undefined) {
            content += data.properties['country-name'];
            content += '\n\n';
        }

        content += formatLocation(data);

        return content;
    }

    if (isCardJson(data)) {
        let content = '';

        if (data.properties['honorific-prefix'] !== undefined) {
            content += data.properties['honorific-prefix'];
            content += ' ';
        }

        if (data.properties['given-name'] !== undefined) {
            content += data.properties['given-name'];
            content += ' ';
        }

        if (data.properties['additional-name'] !== undefined) {
            content += data.properties['additional-name'];
            content += ' ';
        }

        if (data.properties.nickname !== undefined) {
            content += `(${data.properties.nickname})`;
            content += ' ';
        }

        if (data.properties['family-name'] !== undefined) {
            content += data.properties['family-name'];
            content += ' ';
        }

        if (data.properties['honorific-suffix'] !== undefined) {
            content += data.properties['honorific-suffix'];
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        content += formatLocation(data);
        return content;
    }

    if (isEntryJson(data)) {
        let content = '';

        if (data.properties.content !== undefined) {
            content += data.properties.content.map((contentElement) => {
                if (typeof contentElement === 'string') {
                    return contentElement + '\n\n';
                }
                if (contentElement.html !== undefined) {
                    return contentElement.html + '\n\n';
                }
            });
            content += '\n\n';
        }

        if (data.properties.photo !== undefined) {
            content += data.properties.photo.slice(1).map((photo) => {
                if (typeof photo === 'string') {
                    return `![](${photo})\n\n`;
                }
                return `![${photo.alt ?? ''}](${photo.value})\n\n`;
            });
        }

        if (data.properties['in-reply-to'] !== undefined) {
            content += `In reply to: ${data.properties['in-reply-to']}\n\n`;
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        if (data.properties['like-of'] !== undefined) {
            const name = data.properties.name ?? data.properties['like-of'];

            content += `Link: [${name}](${data.properties['like-of']})\n\n`;
        }

        if (data.properties['repost-of'] !== undefined) {
            const name = data.properties.name ?? data.properties['repost-of'];

            content += `Repost of: [${name}](${data.properties['repost-of']})\n\n`;
        }

        if (data.properties['bookmark-of'] !== undefined) {
            const name = data.properties.name ?? data.properties['bookmark-of'];

            content += `Bookmark: [${name}](${data.properties['bookmark-of']})\n\n`;
        }

        if (data.properties['read-of'] !== undefined) {
            const name = data.properties.name ?? data.properties['read-of'];

            content += `Read: [${name}](${data.properties['read-of']})\n\n`;
        }

        if (data.properties.rsvp !== undefined) {
            content += `RSVP: ${data.properties.rsvp}\n\n`;
        }

        if (data.properties.checkin !== undefined) {
            const stubEntry: EntryJson = {
                type: ['h-entry'],
                properties: {
                    location: [data.properties.checkin],
                },
            };

            const urlMatches = data.properties.checkin.match('url=(.*?);');
            const nameMatches =
                data.properties.checkin.match('name=(.*?)(;|$)');

            if (urlMatches !== null) {
                const name = nameMatches ? nameMatches[1] : urlMatches[1];
                content += `Checked in to: [${name}](${urlMatches[1]})\n\n`;
            } else if (nameMatches !== null) {
                content += `Checked in to ${nameMatches[1]}\n\n`;
            }

            content += formatLocation(stubEntry);
        }

        content += formatLocation(data);
        return content;
    }

    if (isEventJson(data)) {
        let content = '';

        if (data.properties.description !== undefined) {
            content += data.properties.description;
            content += '\n\n';
        }

        if (data.properties.location !== undefined) {
            content += `Happening at: ${data.properties.location}\n\n`;
        }

        if (data.properties.start !== undefined) {
            content += `Start Time: ${data.properties.start}\n\n`;
        }

        if (data.properties.end !== undefined) {
            content += `End Time: ${data.properties.end}\n\n`;
        }

        if (data.properties.duration !== undefined) {
            content += `Duration: ${data.properties.duration}\n\n`;
        }

        content += formatLocation(data);

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        return content;
    }

    if (isGeoJson(data)) {
        return formatLocation(data);
    }

    if (isItemJson(data)) {
        let content = '';

        if (data.properties.name !== undefined) {
            content += `Name: ${data.properties.name}\n\n`;
        }

        if (data.properties.photo !== undefined) {
            content += data.properties.photo.slice(1).map((photo) => {
                if (typeof photo === 'string') {
                    return `![](${photo})\n\n`;
                }
                return `![${photo.alt ?? ''}](${photo.value})\n\n`;
            });
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        return content;
    }

    if (isProductJson(data)) {
        let content = '';

        if (data.properties.brand !== undefined) {
            content += `Brand: ${data.properties.brand}\n\n`;
        }

        if (data.properties.price !== undefined) {
            content += `Price: ${data.properties.price}\n\n`;
        }

        if (data.properties.description !== undefined) {
            content += data.properties.description;
            content += '\n\n';
        }

        if (data.properties.review !== undefined) {
            content += data.properties.review;
            content += '\n\n';
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        return content;
    }

    if (isRecipeJson(data)) {
        let content = '';

        if (data.properties.summary !== undefined) {
            content += '## Summary\n\n';
            content += data.properties.summary;
            content += '\n\n';
        }

        if (data.properties.duration !== undefined) {
            content += `Cook Time: ${data.properties.duration}\n\n`;
            content += '\n\n';
        }

        if (data.properties.yield !== undefined) {
            content += `Makes: ${data.properties.yield}\n\n`;
            content += '\n\n';
        }

        if (data.properties.nutrition !== undefined) {
            content += '## Nutrition\n\n';
            content += '\n\n';
        }

        if (data.properties.ingredient !== undefined) {
            content += '## Ingredients\n\n';
            content += data.properties.ingredient;
            content += '\n\n';
        }

        if (data.properties.instructions !== undefined) {
            content += '## Instructions\n\n';
            content += data.properties.instructions;
            content += '\n\n';
        }

        return content;
    }

    if (isResumeJson(data)) {
        let content = '';

        if (data.properties.contact !== undefined) {
            content += '## Contact Details\n\n';
            content += data.properties.contact;
            content += '\n\n';
        }

        if (data.properties.summary !== undefined) {
            content += data.properties.summary;
            content += '\n\n';
        }

        if (data.properties.skill !== undefined) {
            content += '## Skills\n\n';
            content += data.properties.skill;
            content += '\n\n';
        }

        if (data.properties.experience !== undefined) {
            content += '## Experience\n\n';
            content += data.properties.experience;
            content += '\n\n';
        }

        if (data.properties.affiliation !== undefined) {
            content += '## Affiliations\n\n';
            content += data.properties.affiliation;
            content += '\n\n';
        }

        if (data.properties.education !== undefined) {
            content += '## Education\n\n';
            content += data.properties.education;
            content += '\n\n';
        }

        return content;
    }

    if (isReviewJson(data)) {
        let content = '';

        if (data.properties.item !== undefined) {
            content += data.properties.item;
            content += '\n\n';
        }

        if (
            data.properties.rating !== undefined &&
            data.properties.best !== undefined
        ) {
            content += '★'.repeat(data.properties.rating);
            content += '☆'.repeat(
                data.properties.best - data.properties.rating
            );
            content += '\n\n';
        }

        if (data.properties.description !== undefined) {
            content += data.properties.description;
            content += '\n\n';
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        return content;
    }

    if (isReviewAggregateJson(data)) {
        let content = '';

        if (
            data.properties.rating !== undefined &&
            data.properties.best !== undefined
        ) {
            content += '★'.repeat(data.properties.rating);
            content += '☆'.repeat(
                data.properties.best - data.properties.rating
            );
            content += '\n\n';
        }

        if (data.properties.count !== undefined) {
            content += `Number of reviews: ${data.properties.count}\n\n`;
        }

        if (data.properties.votes !== undefined) {
            content += `Number of votes: ${data.properties.votes}\n\n`;
        }

        if (data.properties.url !== undefined) {
            content += `Link: [${data.properties.url}](${data.properties.url})\n\n`;
        }

        return content;
    }

    throw new Error(`Unknown Format`);
}

function formatFrontMatter(data: MicropubJson): string {
    const frontMatter: FrontMatter = {
        draft: false,
        disableComments: true,
        date:
            'published' in data.properties &&
            data.properties.published !== undefined
                ? data.properties.published
                : new Date().toISOString(),
    };

    const locationData = getLocationData(data);

    if (locationData !== undefined) {
        frontMatter.location = {
            latitude: locationData.latitude,
            longitude: locationData.longitude,
        };

        if (locationData.altitude !== undefined) {
            frontMatter.location.altitude = locationData.altitude;
        }
    }

    if ('category' in data.properties) {
        frontMatter.tags = data.properties.category;
    }

    if ('summary' in data.properties) {
        frontMatter.description = data.properties.summary;
    }

    if ('name' in data.properties) {
        frontMatter.title = data.properties.name;
    }

    if ('photo' in data.properties && data.properties.photo !== undefined) {
        frontMatter.image =
            typeof data.properties.photo[0] === 'string'
                ? data.properties.photo[0]
                : data.properties.photo[0].value;
    }

    return JSON.stringify(frontMatter, undefined, 2) + '\n\n';
}

function formatLocation(data: MicropubJson): string {
    const locationData = getLocationData(data);
    if (locationData !== undefined) {
        const { latitude, longitude, altitude } = locationData;

        let content = `Latitude: ${latitude}\n\n`;
        content += `Longitude: ${longitude}\n\n`;

        if (altitude !== undefined) {
            content += `Altitude: ${altitude}\n\n`;
        }

        return (
            content +
            `View on: [Open Street Maps](https://www.openstreetmap.org/?mlat=${latitude}&mlon=${longitude})\n\n`
        );
    }

    return '';
}

interface LocationData {
    altitude?: string;
    latitude: string;
    longitude: string;
}

function getLocationData(data: MicropubJson): LocationData | void {
    if (
        'latitude' in data.properties &&
        'longitude' in data.properties &&
        data.properties.latitude !== undefined &&
        data.properties.longitude !== undefined
    ) {
        const locationData: LocationData = {
            latitude: data.properties.latitude,
            longitude: data.properties.longitude,
            altitude: undefined,
        };

        if ('altitude' in data.properties) {
            locationData.altitude = data.properties.altitude;
        }

        return locationData;
    }

    if (
        'location' in data.properties &&
        data.properties.location !== undefined
    ) {
        const [latitude, longitude, altitude] = data.properties.location[0]
            .replace(/^geo:/, '')
            .split(',');

        return {
            latitude,
            longitude,
            altitude,
        };
    }
}
