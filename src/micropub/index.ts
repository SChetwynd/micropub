import express from 'express';

import { createRouter } from './routes.js';
import { documentation } from './api-documentation.js';
import { IndieAuth, Gitlab, Parameters } from './types';
import { buildValidation, Validation } from './validation/index.js';

export class Micropub {
    indieAuth: IndieAuth;
    gitlab: Gitlab;
    logger: (message: string) => void;
    config: Config;

    constructor({ indieAuth, gitlab, createLogger, config }: Parameters) {
        this.indieAuth = indieAuth;
        this.gitlab = gitlab;
        this.logger = createLogger('MICROPUB');
        this.config = config;
    }

    get router(): express.Router {
        return createRouter(this);
    }

    get documentation() {
        return documentation;
    }

    get validation(): Validation {
        return buildValidation(this.config);
    }
}

let micropub: Micropub;
export function createMicropub(parameters: Parameters): Micropub {
    if (micropub === undefined) {
        micropub = new Micropub(parameters);
    }
    return micropub;
}
