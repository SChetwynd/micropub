export const documentation = {
    '/v1/micropub': {
        post: {
            description: 'Micropub endpoint for creating new posts',
            parameters: [
                {
                    description: 'The auth header',
                    name: 'authorization',
                    in: 'header',
                    schema: {
                        type: 'string',
                    },
                    required: false,
                },
            ],
            requestBody: {
                content: {
                    'application/x-www-form-urlencoded': {
                        schema: {
                            type: 'obejct',
                            properties: {
                                h: {
                                    type: 'string',
                                    enum: [
                                        'adr',
                                        'card',
                                        'entry',
                                        'event',
                                        'feed',
                                        'geo',
                                        'item',
                                        'product',
                                        'recipe',
                                        'resume',
                                        'review',
                                        'review-aggregate',
                                    ],
                                },
                            },
                        },
                    },
                },
            },
            responses: {
                '201': {
                    description: 'The post has been created',
                    headers: {
                        Location: {
                            type: 'string',
                            description:
                                'A permalink to the newly created post',
                        },
                        Link: {
                            type: 'string',
                            description:
                                'The URL and "rel" of any syndication of the post',
                        },
                    },
                },
                '202': {
                    description:
                        'The post has been successfully accepted, and will be' +
                        ' process syncronously. This is not a guarentee that ' +
                        'the post will be created as errors could occor after' +
                        ' this point',
                    headers: {
                        Location: {
                            type: 'string',
                            description:
                                'A permalink to where the post will be created',
                        },
                        Link: {
                            type: 'string',
                            description:
                                'The URL and "rel" of any syndication of the post',
                        },
                    },
                },
                '403': {
                    description:
                        'The authenticated user does not have permission to ' +
                        'perform this request',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                required: ['error'],
                                properties: {
                                    error: {
                                        type: 'string',
                                        const: 'forbidden',
                                    },
                                },
                            },
                        },
                    },
                },
                '401': {
                    description:
                        'The request either lacked an authentication token' +
                        ' or the token lacked sufficient scope to perform ' +
                        'action.',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                required: ['error'],
                                properties: {
                                    error: {
                                        type: 'string',
                                        enum: [
                                            'unauthorized',
                                            'insufficient_scope',
                                        ],
                                    },
                                    scope: {
                                        description:
                                            'Optional property describing ' +
                                            'the required scope for the action',
                                        type: 'string',
                                    },
                                },
                            },
                        },
                    },
                },
                '400': {
                    description:
                        'The request is missing a required parameter, or ' +
                        'there was a problem with a value of one of the parameters',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {
                                    error: {
                                        type: 'string',
                                        const: 'invalid_request',
                                    },
                                },
                            },
                        },
                    },
                },
                '500': {
                    description: 'Something unexpected went wrong.',
                },
            },
        },
    },
};
