import {
    Micropub,
    MicropubJson,
    AdrForm,
    AdrJson,
    CardForm,
    CardJson,
    EntryForm,
    EntryJson,
    EventForm,
    EventJson,
    GeoForm,
    GeoJson,
    ItemForm,
    ItemJson,
    ProductForm,
    ProductJson,
    RecipeForm,
    RecipeJson,
    ResumeForm,
    ResumeJson,
    ReviewForm,
    ReviewJson,
    ReviewAggregateForm,
    ReviewAggregateJson,
} from './types';

import {
    isMicropubJson,
    isAdrForm,
    isCardForm,
    isEntryForm,
    isEventForm,
    isGeoForm,
    isItemForm,
    isProductForm,
    isRecipeForm,
    isResumeForm,
    isReviewForm,
    isReviewAggregateForm,
} from './guards.js';

export function standardiseBody(body: Micropub): MicropubJson {
    if (isMicropubJson(body)) {
        return body;
    }

    if (isAdrForm(body)) {
        return standardiseAdr(body);
    }

    if (isCardForm(body)) {
        return standardiseCard(body);
    }

    if (isEntryForm(body)) {
        return standardiseEntry(body);
    }

    if (isEventForm(body)) {
        return standardiseEvent(body);
    }

    if (isGeoForm(body)) {
        return standardiseGeo(body);
    }

    if (isItemForm(body)) {
        return standardiseItem(body);
    }

    if (isProductForm(body)) {
        return standardiseProduct(body);
    }

    if (isRecipeForm(body)) {
        return standardiseRecipe(body);
    }

    if (isResumeForm(body)) {
        return standardiseResume(body);
    }

    if (isReviewForm(body)) {
        return standardiseReview(body);
    }

    if (isReviewAggregateForm(body)) {
        return standardiseReviewAggregate(body);
    }

    throw new Error('Unknown body type');
}

function standardiseAdr(body: AdrForm): AdrJson {
    const adr: AdrJson = {
        type: ['h-adr'],
        properties: {},
    };

    if (body['post-office-box'] !== undefined) {
        adr.properties['post-office-box'] = body['post-office-box'];
    }

    if (body['extended-address'] !== undefined) {
        adr.properties['extended-address'] = body['extended-address'];
    }

    if (body['street-address'] !== undefined) {
        adr.properties['street-addres'] = body['street-address'];
    }

    if (body.locality !== undefined) {
        adr.properties.locality = body.locality;
    }

    if (body.region !== undefined) {
        adr.properties.region = body.region;
    }

    if (body['postal-code'] !== undefined) {
        adr.properties['postal-code'] = body['postal-code'];
    }

    if (body['country-name'] !== undefined) {
        adr.properties['country-name'] = body['country-name'];
    }

    if (body.label !== undefined) {
        adr.properties.label = body.label;
    }

    if (body.latitude !== undefined) {
        adr.properties.latitude = body.latitude;
    }

    if (body.longitude !== undefined) {
        adr.properties.longitude = body.longitude;
    }

    if (body.altitude !== undefined) {
        adr.properties.altitude = body.altitude;
    }

    return adr;
}

export function standardiseCard(body: CardForm): CardJson {
    const card: CardJson = {
        type: ['h-card'],
        properties: {},
    };

    if (body.name !== undefined) {
        card.properties.name = body.name;
    }

    if (body['honorific-prefix'] !== undefined) {
        card.properties['honorific-prefix'] = body['honorific-prefix'];
    }

    if (body['given-name'] !== undefined) {
        card.properties['given-name'] = body['given-name'];
    }

    if (body['additional-name'] !== undefined) {
        card.properties['additional-name'] = body['additional-name'];
    }

    if (body['family-name'] !== undefined) {
        card.properties['family-name'] = body['family-name'];
    }

    if (body['sort-string'] !== undefined) {
        card.properties['sort-string'] = body['sort-string'];
    }

    if (body['honorific-suffix'] !== undefined) {
        card.properties['honorific-suffix'] = body['honorific-suffix'];
    }

    if (body.nickname !== undefined) {
        card.properties.nickname = body.nickname;
    }

    if (body.email !== undefined) {
        card.properties.email = body.email;
    }

    if (body.logo !== undefined) {
        card.properties.logo = body.logo;
    }

    if (body.photo !== undefined && body.photo !== undefined) {
        card.properties.photo = Array.isArray(body.photo)
            ? body.photo
            : [body.photo];
    }

    if (body.url !== undefined) {
        card.properties.url = body.url;
    }

    if (body.uid !== undefined) {
        card.properties.uid = body.uid;
    }

    if (body.category !== undefined) {
        card.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.adr !== undefined) {
        card.properties.adr = body.adr;
    }

    if (body['post-office-box'] !== undefined) {
        card.properties['post-office-box'] = body['post-office-box'];
    }

    if (body['extended-address'] !== undefined) {
        card.properties['extended-address'] = body['extended-address'];
    }

    if (body['street-address'] !== undefined) {
        card.properties['street-address'] = body['street-address'];
    }

    if (body.locality !== undefined) {
        card.properties.locality = body.locality;
    }

    if (body.region !== undefined) {
        card.properties.region = body.region;
    }

    if (body['postal-code'] !== undefined) {
        card.properties['postal-code'] = body['postal-code'];
    }

    if (body['contry-name'] !== undefined) {
        card.properties['contry-name'] = body['contry-name'];
    }

    if (body.label !== undefined) {
        card.properties.label = body.label;
    }

    if (body.geo !== undefined) {
        card.properties.geo = body.geo;
    }

    if (body.latitude !== undefined) {
        card.properties.latitude = body.latitude;
    }

    if (body.longitude !== undefined) {
        card.properties.longitude = body.longitude;
    }

    if (body.altitude !== undefined) {
        card.properties.altitude = body.altitude;
    }

    if (body.tel !== undefined) {
        card.properties.tel = body.tel;
    }

    if (body.note !== undefined) {
        card.properties.note = body.note;
    }

    if (body.bday !== undefined) {
        card.properties.bday = body.bday;
    }

    if (body.key !== undefined) {
        card.properties.key = body.key;
    }

    if (body['job-title'] !== undefined) {
        card.properties['job-title'] = body['job-title'];
    }

    if (body.role !== undefined) {
        card.properties.role = body.role;
    }

    return card;
}

function standardiseEntry(body: EntryForm): EntryJson {
    const entry: EntryJson = {
        type: ['h-entry'],
        properties: {},
    };

    if (body.name !== undefined) {
        entry.properties.name = body.name;
    }

    if (body.summary !== undefined) {
        entry.properties.summary = body.summary;
    }

    if (body.content !== undefined && body.content !== undefined) {
        entry.properties.content = Array.isArray(body.content)
            ? body.content
            : [body.content];
    }

    if (body.published !== undefined) {
        entry.properties.published = body.published;
    }

    if (body.updated !== undefined) {
        entry.properties.updated = body.updated;
    }

    if (body.author !== undefined) {
        entry.properties.author = body.author;
    }

    if (body.location !== undefined) {
        entry.properties.location = [body.location];
    }

    if (body.category !== undefined) {
        entry.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.url !== undefined) {
        entry.properties.url = body.url;
    }

    if (body.geo !== undefined) {
        entry.properties.geo = body.geo;
    }

    if (body.latitude !== undefined) {
        entry.properties.latitude = body.latitude;
    }

    if (body.longitude !== undefined) {
        entry.properties.longitude = body.longitude;
    }

    if (body.photo !== undefined) {
        entry.properties.photo = Array.isArray(body.photo)
            ? body.photo
            : [body.photo];
    }

    if (body.audio !== undefined) {
        entry.properties.audio = body.audio;
    }

    if (body.video !== undefined) {
        entry.properties.video = body.video;
    }

    if (body['in-reply-to'] !== undefined) {
        entry.properties['in-reply-to'] = body['in-reply-to'];
    }

    if (body['repost-of'] !== undefined) {
        entry.properties['repost-of'] = body['repost-of'];
    }

    if (body['bookmark-of'] !== undefined) {
        entry.properties['bookmark-of'] = body['bookmark-of'];
    }

    if (body['read-of'] !== undefined) {
        entry.properties['read-of'] = body['read-of'];
    }

    if (body.rsvp !== undefined) {
        entry.properties.rsvp = body.rsvp;
    }

    return entry;
}

function standardiseEvent(body: EventForm): EventJson {
    const event: EventJson = {
        type: ['h-event'],
        properties: {},
    };

    if (body.name !== undefined) {
        event.properties.name = body.name;
    }

    if (body.summary !== undefined) {
        event.properties.summary = body.summary;
    }

    if (body.start !== undefined) {
        event.properties.start = body.start;
    }

    if (body.end !== undefined) {
        event.properties.end = body.end;
    }

    if (body.duration !== undefined) {
        event.properties.duration = body.duration;
    }

    if (body.description !== undefined) {
        event.properties.description = body.description;
    }

    if (body.url !== undefined) {
        event.properties.url = body.url;
    }

    if (body.category !== undefined) {
        event.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.location !== undefined) {
        event.properties.location = body.location;
    }

    if (body.geo !== undefined) {
        event.properties.geo = body.geo;
    }

    if (body.latitude !== undefined) {
        event.properties.latitude = body.latitude;
    }

    if (body.longitude !== undefined) {
        event.properties.longitude = body.longitude;
    }

    return event;
}

function standardiseGeo(body: GeoForm): GeoJson {
    const geo: GeoJson = {
        type: ['h-geo'],
        properties: {},
    };

    if (body.latitude !== undefined) {
        geo.properties.latitude = body.latitude;
    }

    if (body.longitude !== undefined) {
        geo.properties.longitude = body.longitude;
    }

    if (body.altitude !== undefined) {
        geo.properties.altitude = body.altitude;
    }

    return geo;
}

function standardiseItem(body: ItemForm): ItemJson {
    const item: ItemJson = {
        type: ['h-item'],
        properties: {},
    };

    if (body.name !== undefined) {
        item.properties.name = body.name;
    }

    if (body.photo !== undefined) {
        item.properties.photo = Array.isArray(body.photo)
            ? body.photo
            : [body.photo];
    }

    if (body.url !== undefined) {
        item.properties.url = body.url;
    }

    return item;
}

function standardiseProduct(body: ProductForm): ProductJson {
    const product: ProductJson = {
        type: ['h-product'],
        properties: {},
    };

    if (body.name !== undefined) {
        product.properties.name = body.name;
    }

    if (body.photo !== undefined) {
        product.properties.photo = Array.isArray(body.photo)
            ? body.photo
            : [body.photo];
    }

    if (body.brand !== undefined) {
        product.properties.brand = body.brand;
    }

    if (body.category !== undefined) {
        product.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.description !== undefined) {
        product.properties.description = body.description;
    }

    if (body.url !== undefined) {
        product.properties.url = body.url;
    }

    if (body.identifier !== undefined) {
        product.properties.identifier = body.identifier;
    }

    if (body.review !== undefined) {
        product.properties.review = body.review;
    }

    if (body.price !== undefined) {
        product.properties.price = body.price;
    }

    return product;
}

function standardiseRecipe(body: RecipeForm): RecipeJson {
    const recipe: RecipeJson = {
        type: ['h-recipe'],
        properties: {},
    };

    if (body.ingredient !== undefined) {
        recipe.properties.ingredient = body.ingredient;
    }

    if (body.yield !== undefined) {
        recipe.properties.yield = body.yield;
    }

    if (body.instructions !== undefined) {
        recipe.properties.instructions = body.instructions;
    }

    if (body.duration !== undefined) {
        recipe.properties.duration = body.duration;
    }

    if (body.photo !== undefined) {
        recipe.properties.photo = Array.isArray(body.photo)
            ? body.photo
            : [body.photo];
    }

    if (body.summary !== undefined) {
        recipe.properties.summary = body.summary;
    }

    if (body.author !== undefined) {
        recipe.properties.author = body.author;
    }

    if (body.published !== undefined) {
        recipe.properties.published = body.published;
    }

    if (body.nutrition !== undefined) {
        recipe.properties.nutrition = body.nutrition;
    }

    return recipe;
}

function standardiseResume(body: ResumeForm): ResumeJson {
    const resume: ResumeJson = {
        type: ['h-resume'],
        properties: {},
    };

    if (body.summary !== undefined) {
        resume.properties.summary = body.summary;
    }

    if (body.contact !== undefined) {
        resume.properties.contact = body.contact;
    }

    if (body.education !== undefined) {
        resume.properties.education = body.education;
    }

    if (body.experience !== undefined) {
        resume.properties.experience = body.experience;
    }

    if (body.skill !== undefined) {
        resume.properties.skill = body.skill;
    }

    if (body.affiliation !== undefined) {
        resume.properties.affiliation = body.affiliation;
    }

    return resume;
}

function standardiseReview(body: ReviewForm): ReviewJson {
    const review: ReviewJson = {
        type: ['h-review'],
        properties: {},
    };

    if (body.name !== undefined) {
        review.properties.name = body.name;
    }

    if (body.item !== undefined) {
        review.properties.item = body.item;
    }

    if (body.reviewer !== undefined) {
        review.properties.reviewer = body.reviewer;
    }

    if (body.reviewed !== undefined) {
        review.properties.reviewed = body.reviewed;
    }

    if (body.rating !== undefined) {
        review.properties.rating = body.rating;
    }

    if (body.best !== undefined) {
        review.properties.best = body.best;
    }

    if (body.worst !== undefined) {
        review.properties.worst = body.worst;
    }

    if (body.description !== undefined) {
        review.properties.description = body.description;
    }

    if (body.category !== undefined) {
        review.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.url !== undefined) {
        review.properties.url = body.url;
    }

    return review;
}

function standardiseReviewAggregate(
    body: ReviewAggregateForm
): ReviewAggregateJson {
    const reviewAggregate: ReviewAggregateJson = {
        type: ['h-review-aggregate'],
        properties: {},
    };

    if (body.name !== undefined) {
        reviewAggregate.properties.name = body.name;
    }

    if (body.item !== undefined) {
        reviewAggregate.properties.item = body.item;
    }

    if (body.rating !== undefined) {
        reviewAggregate.properties.rating = body.rating;
    }

    if (body.best !== undefined) {
        reviewAggregate.properties.best = body.best;
    }

    if (body.worst !== undefined) {
        reviewAggregate.properties.worst = body.worst;
    }

    if (body.count !== undefined) {
        reviewAggregate.properties.count = body.count;
    }

    if (body.votes !== undefined) {
        reviewAggregate.properties.votes = body.votes;
    }

    if (body.category !== undefined) {
        reviewAggregate.properties.category = Array.isArray(body.category)
            ? body.category
            : [body.category];
    }

    if (body.url !== undefined) {
        reviewAggregate.properties.url = body.url;
    }

    return reviewAggregate;
}

export function convertArrays(
    body: Record<string, unknown>
): Record<string, unknown> {
    const convertedBody: Record<string, unknown> = {};
    for (const [key, value] of Object.entries(body)) {
        if (/\[]$/.test(key)) {
            convertedBody[key.replace(/\[]$/, '')] = value;
        } else {
            convertedBody[key] = value;
        }
    }

    return convertedBody;
}
