import fs from 'node:fs';

export function createGitLabHelper(
    token: string,
    projectId: number,
    branch: string,
    createLogger: (type: string) => (message: string) => void
): {
    createCommit: (body: string) => Promise<string>;
    uploadFiles: (
        fileArray: { content: string; fileName: string }[]
    ) => Promise<string[]>;
} {
    const logger = createLogger('AUTH');
    return {
        async uploadFiles(
            fileArray: { content: string; fileName: string }[]
        ): Promise<string[]> {
            const isoDate = new Date().toISOString();

            const filePaths = [];
            const actions = [];

            for (const { content, fileName } of fileArray) {
                const filePath = `static/micropub/${isoDate}-${fileName}`;
                filePaths.push(filePath);
                actions.push({
                    action: 'create',
                    file_path: filePath,
                    content: content,
                    encoding: 'base64',
                });
            }

            const response = await fetch(
                `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
                {
                    method: 'POST',
                    headers: new Headers({
                        'PRIVATE-TOKEN': token,
                        'Content-Type': 'application/json',
                    }),
                    body: JSON.stringify({
                        branch,
                        commit_message: '[skip-ci] Media-Endpoint',
                        actions,
                    }),
                }
            );

            if (response.status < 200 || response.status >= 300) {
                logger(
                    `Commit failed: ${response.status} - ${response.statusText}`
                );
                throw new Error(
                    'Invalid response status code when posting to gitlab'
                );
            }

            return filePaths;
        },
        async createCommit(body: string): Promise<string> {
            const isoDate = new Date().toISOString();

            const filePath = `content/micropub/${isoDate}-micropub.md`;

            const response = await fetch(
                `https://gitlab.com/api/v4/projects/${projectId}/repository/commits`,
                {
                    method: 'POST',
                    headers: new Headers({
                        'PRIVATE-TOKEN': token,
                        'Content-Type': 'application/json',
                    }),
                    body: JSON.stringify({
                        branch,
                        commit_message: 'micropub',
                        actions: [
                            {
                                action: 'create',
                                file_path: filePath,
                                content: body,
                            },
                        ],
                    }),
                }
            );

            if (response.status < 200 || response.status >= 300) {
                logger(
                    `Commit failed: ${response.status} - ${response.statusText}`
                );
                throw new Error(
                    'Invalid response status code when posting to gitlab'
                );
            }

            return filePath;
        },
    };
}
