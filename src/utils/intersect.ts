/**
 * Tests if all elements of the second array are present in the first
 * @param array1 array of unknown elements
 * @param array2 array of unknown elements all of which should be present in array1
 * @returns true if all elements are present
 */
export function intersect(array1: unknown[], array2: unknown[]): boolean {
    const set1 = new Set<unknown>(array1);
    return !array2.some((element) => !set1.has(element));
}
