export function singletonise<T>(
    function_: (...arguments_: unknown[]) => T
): (...arguments_: unknown[]) => T {
    let instance: T;
    return (...arguments_) => {
        if (instance === undefined) {
            instance = function_(...arguments_);
        }
        return instance;
    };
}
