declare interface Config {
    projectId: number;
    port: number;
    token: string;
    branch: string;
    host: string;
    me: string;
    sentryDSN: string | undefined;
    mediaEndpoint?: string;
    tokenEndpoint: string;
}

declare interface CustomError {
    type: string;
}

declare interface PostForm {
    h: 'entry' | 'card' | 'event' | 'cite';
    name?: string;
    summary?: string;
    content: string;
    published?: string;
    updated?: string;
    category?: string;
    'category[]'?: string;
    location?: string;
    'in-reply-to'?: string;
    'repost-of'?: string;
    syndication?: string | string[];
    'syndication[]'?: string;
    'mp-syndicate-to'?: string | string[];
    'mp-syndicate-to[]'?: string | string[];
    photo?: string;
}
