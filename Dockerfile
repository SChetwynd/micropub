FROM node:slim
WORKDIR /app
COPY ./package.json ./package.json
RUN npm install

COPY ./tsconfig.json ./tsconfig.json
COPY ./types ./types
COPY ./src ./src
RUN npm run build

EXPOSE 3000
ENTRYPOINT ["npm", "run", "start"]
